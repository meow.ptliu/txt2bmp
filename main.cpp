#include <fstream>

#include "./bitmap.h"

int main(int argc, char *argv[]) {
    if (argc == 2) {
        std::ofstream file(argv[1], std::ofstream::trunc | std::ofstream::binary);
        Bitmap image;
        image.read_text();
        image.config();
        file << image;
        file.close();
    }
}

#include "bitmap.h"

#include <cmath>
#include <exception>
#include <iostream>

uint32_t get_width(double string_length) {
    double mid = std::ceil(std::sqrt(string_length));
    if (std::isfinite(mid))
        return mid;
    throw std::runtime_error("String length is invalid!");
}

void to_bytes(uint8_t* head, uint32_t data) noexcept {
    for (int i = 0; i < 4; i++) {
        *(head++) = data & 255;
        data >>= 8;
    }
}

std::ostream& operator<<(std::ostream& out, const Bitmap& bmp) {
    out.write(reinterpret_cast<const char*>(&bmp.file_header), 14);
    out.write(reinterpret_cast<const char*>(&bmp.info_header), 40);
    out.put('\n');
    out.write(reinterpret_cast<const char*>(bmp.text.data()), bmp.text.size() - 1);
    for (size_t i = 0; i < bmp.padding - 1; ++i)
        out.put(' ');
    out.put('\n');
    return out;
}

void Bitmap::read_text() {
    while (!std::cin.eof())
        text.emplace_back(std::cin.get());
}

void Bitmap::config() {
    uint32_t size = text.size();
    uint32_t rows = get_width(static_cast<double>(size) / 4);
    uint32_t pixels = rows * rows;
    to_bytes(file_header.size, pixels * 4 + 54);
    to_bytes(info_header.width, rows);
    to_bytes(info_header.height, rows);
    to_bytes(info_header.imagesize, pixels);
    padding = pixels * 4 - size;
}

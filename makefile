all: bitmap.o main.o
	g++ -O2 -Wall -o text2bmp.out main.o bitmap.o
bitmap.o: bitmap.cpp bitmap.h
	g++ -O2 -Wall -c bitmap.cpp
txt2bmp.o: main.cpp
	g++ -O2 -Wall -c main.cpp
clean:
	rm main.o bitmap.o

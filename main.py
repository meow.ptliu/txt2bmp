"""
LICENSE: MIT license

A module that can code plain text in bitmap image.
Support UTF-8 codec string.
"""

import math
import random
import struct
import sys


class Bitmap:
    """Code plain text into a bitmap image

    Attributes:
        data:       (bytes) Encoded text from data.
        width:      (int)   Width of the image.
        height:     (int)   Height of the image.
        pixels:     (int)   Total pixels in the bitmap (32 bits bitmap).
        padding:    (bytes) Pad the data to fill all the pixels.
        filename:   (str)   Filename of the output bitmap.
    """

    def __init__(self, data: bytes, output="output.bmp"):
        self.data = data.encode("utf8")
        if not self.data:
            print("Error!")
            print("Reason: No text found!")
            sys.exit(1)
        self.width = math.ceil(math.sqrt(len(self.data) / 4))
        # +1 for the additional '\n'
        self.height = math.ceil((len(self.data) + 1) / (self.width * 4))
        self.pixels = self.height * self.width * 4
        self.padding = struct.pack("<{}x".format(self.pixels - len(self.data)))
        self.filename = output
        self.header = struct.pack("<2sI4x4I2H4xI16xB",
                                  b"BM", self.pixels + 54, 54,  # file_header
                                  40, self.width, self.height, 1, 32, self.pixels,  # info_header
                                  10)  # Add a '\n' at the end to prettify text

    def write(self):
        """Write header, data, padding sequentially"""
        with open(self.filename, "wb") as image:
            image.writelines((self.header, self.data, self.padding))

    def change_pad(self, padding_string="", random_color=False):
        """Change the text pads in the end, default is '\0'

        Args:
            padding_string: (str)  New padding string.
            random_color:   (bool) If true, ignore other parameter and generate random data.
        """
        length = len(self.padding)
        if random_color:
            self.padding = struct.pack("<{}B".format(length),
                                       *(random.randint(0, 255) for _ in range(length)))
        else:
            padding_string = padding_string.encode("utf8")
            multiplier = math.ceil(length / len(padding_string))
            self.padding = padding_string * multiplier


def main():
    """Avoid execution when being import as module"""
    image = Bitmap(sys.stdin.read())
    image.change_pad(random_color=True)
    image.write()


if __name__ == "__main__":
    main()

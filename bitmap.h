#ifndef TXT2BMP_BITMAT_H_
#define TXT2BMP_BITMAT_H_

#include <cstdint>
#include <fstream>
#include <vector>

uint32_t get_width(double string_length);
void to_bytes(uint8_t* head, uint32_t data) noexcept;

struct FileHeader {
    uint8_t type[2] = {66, 77};
    uint8_t size[4];
    uint8_t reserved[4] = {0, 0, 0, 0};
    uint8_t offset[4] = {54, 0, 0, 0};
};

struct InfoHeader {
    uint8_t size[4] = {40, 0, 0, 0};
    uint8_t width[4];
    uint8_t height[4];
    uint8_t planes[2] = {1, 0};
    uint8_t bits[2] = {32, 0};
    uint8_t compression[4] = {0, 0, 0, 0};
    uint8_t imagesize[4];
    uint8_t xresolution[4] = {0, 0, 0, 0};
    uint8_t yresolution[4] = {0, 0, 0, 0};
    uint8_t ncolours[4] = {0, 0, 0, 0};
    uint8_t importantcolours[4] = {0, 0, 0, 0};
};

class Bitmap {
 private:
    size_t padding;
    std::vector<uint8_t> text;
    FileHeader file_header;
    InfoHeader info_header;

 public:
    Bitmap() = default;
    Bitmap(const Bitmap&) = delete;
    Bitmap(Bitmap&&) = delete;
    friend std::ostream& operator<<(std::ostream& out, const Bitmap& bmp);
    void read_text();
    void config();
};

#endif  // TXT2BMP_BITMAT_H_
